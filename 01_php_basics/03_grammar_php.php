<?php
    // 头信息
    header('content-type:text/html;charset=utf-8');

    // ------------------ 条件语句
    // $score = 80;
    // if($score < 60){
    //     $result = '不及格';
    // }elseif($score < 70){
    //     $result = '及格';
    // }else{
    //     $result = "优秀";
    // }
    // echo "<h1>$result</h1>";

    // --------------------- php中的数组

    // --------- 数值型数组
    // demo1:js数组：  var colors = ['red','blue','black']
    // $colors = array('red','blue','black');
    // $colors = ['red','blue','black'];
    // echo $colors[1];

    // var_dump可以打印数据的详细信息
    // var_dump($colors); //array(3) { [0]=> string(3) "red" [1]=> string(4) "blue" [2]=> string(5) "black" }

    // json_encode() 可以把php的数组转为符合js语法的json字符串
    // echo json_encode($colors); //["red","blue","black"]

    // demo2: js数组：  var colors = ['red','blue','black']
    // $colors[0] = 'red';
    // $colors[1] = 'blue';
    // $colors[2] = 'black';
    // echo $colors[2];


    // demo3: js数组：  var colors = ['red','blue','black']
    // $colors[] = 'red';
    // $colors[] = 'blue';
    // $colors[] = 'black';
    // echo $colors[2] .'<br>';
    // var_dump($colors);
    // echo  '<br>  json字符串:  ' . json_encode($colors);

    // ------------ php 关联数组
    // demo1:  js对象  
    // var person = {
    //   name: 'Mary',
    //   age: 23,
    //   address: '郑州'
    // }

    // $person =['name' => 'Mary', 'age' => 23, 'address' => '郑州'];
    // echo $person['address'] . '<br>';
    // 数据详细信息
    // var_dump($person); // array(3) { ["name"]=> string(4) "Mary" ["age"]=> int(23) ["address"]=> string(6) "郑州" }
    // json字符串
    // echo '<br>' . json_encode($person); // {"name":"Mary","age":23,"address":"\u90d1\u5dde"}

    
    // demo2
    // $person["name"] = "Mary";
    // $person["age"] = 23;
    // $person["address"] = "郑州";
    // echo $person["address"];

    // ---------- 多维数组
    // demo1:  js中的json对象
    // var cartList = [
    //   {
    //     id: 1,
    //     proName: '手机',
    //     price: 300
    //   },
    //   {
    //     id: 3,
    //     proName: '水杯',
    //     price: 43
    //   }
    // ]
    
    // $cartList[] = ['id' => 1, 'proName' => '手机', 'price' => 300];
    // $cartList[] = ['id' => 3, 'proName' => '水杯', 'price' => 43];

    // echo $cartList[0]["proName"] . '<br>'; // 手机
    // var_dump($cartList); // array(2) { [0]=> array(3) { ["id"]=> int(1) ["proName"]=> string(6) "手机" ["price"]=> int(300) } [1]=> array(3) { ["id"]=> int(3) ["proName"]=> string(6) "水杯" ["price"]=> int(43) } }
    // echo '<br>  json字符串：  ' . json_encode($cartList);

    // demo2: js中的json对象
    //   var result = {
    //     code: 0,
    //     cartList: [
    //       {
    //         id: 1,
    //         proName: '手机',
    //         price: 300
    //       },
    //       {
    //         id: 3,
    //         proName: '水杯',
    //         price: 43
    //       }
    //     ]
    //   }
    
    // $result['code'] = 0;
    // $result['cartList'][] = ['id' => 1, 'proName' => '手机', 'price' => 300];
    // $result['cartList'][] = ['id' => 3, 'proName' => '水杯', 'price' => 43];

    // var_dump($result); // array(2) { ["code"]=> int(0) ["cartList"]=> array(2) { [0]=> array(3) { ["id"]=> int(1) ["proName"]=> string(6) "手机" ["price"]=> int(300) } [1]=> array(3) { ["id"]=> int(3) ["proName"]=> string(6) "水杯" ["price"]=> int(43) } } }
    // echo '<br> result["cartList"]的json字符串 ： ' . json_encode($result['cartList']); //[{"id":1,"proName":"\u624b\u673a","price":300},{"id":3,"proName":"\u6c34\u676f","price":43}]
    // echo '<br> result的json字符串 :  ' . json_encode($result); // 

    // --------------- php数组遍历方法

    $colors = array('red','blue','black');

    // count()返回数组的长度
    // for($i = 0; $i < count($colors); $i++){
    //     echo $colors[$i] . '<br>';
    // }

    // foreach($colors as $key => $value){
    //     echo $key ." => ". $value . '<br>';
    // }

    $person = array("name"=>"Mary","age"=>23,"address"=>"郑州");

    foreach($person as $key => $value){
        echo $key ." => ". $value .'<br>';
    }
?>