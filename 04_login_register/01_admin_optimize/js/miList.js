let cont = $(".cont");

// 渲染数据
function render(data) {
    let html = "";
    if (data.code == 0) {
        data.list.forEach(item => {
            html += `
                <tr>
                    <td>${item.Id}</td>
                    <td>${item.title}</td>
                    <td><img src="https:${item.imgUrl}" alt=""></td>
                    <td>${item.hot}</td>
                    <td><button class='edit' data-id=${item.Id}>编辑</button></td>
                    <td><button class='del' data-id=${item.Id}>删除</button></td>
                </tr>
            `;
        })
    } else {
        html = `
            <tr>暂无数据</tr>
        `
    }
    cont.innerHTML = html;
}

// 初始化页面加载数据
function loadAll() {
    let url = "selectAll.php";
    ajaxRequest(url, render);
}

// 搜索
function selectTitle() {
    let searchBtn = $(".toolbar .search-btn");
    let searchBox = $(".toolbar .search-box");

    searchBtn.onclick = function(e) {
        e.preventDefault();
        let titleVal = searchBox.value;
        let url = `select_title.php?title=${titleVal}`;
        ajaxRequest(url, render);
    }
}

// 删除数据渲染
function delRender(data) {
    if (data.code == 0) {
        loadAll();
        alert("成功移除数据！");
    } else {
        alert("你没有权限！");
    }
}

// 删除
function delData() {
    var delbtn = function(e) {
        if (e.target.classList.contains("del")) {
            let id = e.target.dataset.id;
            let url = `del.php?id=${id}`;
            ajaxRequest(url, delRender);
        }
    }
    cont.addEventListener("click", delbtn);
}

// 添加渲染数据
function addRender(data) {
    // 判断是否成功添加数据
    if (data.code == 0) {
        loadAll();
        alert("成功添加数据！");
    } else {
        alert("你没有权限！");
    }
}

// 添加数据
function addData() {
    var hideBtn = $(".toolbar .add");
    var addBtn = $(".add-pannel .add-btn");
    var editBtn = $(".add-pannel .edit-btn");
    var title = $(".add-pannel .title");
    var imgUrl = $(".add-pannel .imgUrl");
    var hotCheck = $(".add-pannel .hot1");
    let titleH2 = $(".add-pannel h2");
    let form = $(".add-pannel");

    hideBtn.onclick = function() {
        addBtn.style.display = "block";
        editBtn.style.display = "none";
        titleH2.innerHTML = "添加商品";
        $(".mask").classList.add("active");
        $(".add-pannel").classList.add("active");
    }

    let addComBtn = function(e) {
        e.preventDefault();
        let titleVal = title.value;
        let imgUrlVal = imgUrl.value;
        let hot = hotCheck.checked ? 1 : 0;
        let url = `add.php?title=${titleVal}&imgUrl=${imgUrlVal}&hot=${hot}`;
        ajaxRequest(url, addRender);
        $(".mask").classList.remove("active");
        $(".add-pannel").classList.remove("active");
        form.reset();
    }
    addBtn.addEventListener("click", addComBtn);

}

function editRenter(data) {
    // 判断是否成功添加数据
    if (data.code == 0) {
        loadAll();
        alert("成功修改数据！");
    } else {
        alert("你没有权限！");
    }
}

// 编辑数据
function update() {
    var addBtn = $(".add-pannel .add-btn");
    var editBtn = $(".add-pannel .edit-btn");
    var title = $(".add-pannel .title");
    var imgUrl = $(".add-pannel .imgUrl");
    var hot1 = $(".add-pannel .hot1");
    let hot2 = $(".add-pannel .hot2");
    let titleH2 = $(".add-pannel h2");
    let form = $(".add-pannel");
    let id;

    // 把原有信息显示在表单中
    let editHandle = function(e) {
        if (e.target.classList.contains("edit")) {

            // 初始化编辑的弹出框
            addBtn.style.display = "none";
            editBtn.style.display = "block";
            titleH2.innerHTML = "编辑商品";

            // 显示隐藏框 
            $(".mask").classList.add("active");
            $(".add-pannel").classList.add("active");

            id = e.target.dataset.id;

            // 获取商品的值
            var trEl = e.target.parentNode.parentNode;
            let tdList = trEl.querySelectorAll("td");
            let titleVal = tdList[1].innerText;
            let imgUrlVal = tdList[2].children[0].src.substr(6);
            let hotVal = tdList[3].innerText;

            // 为隐藏中的输入框添加内容
            title.value = titleVal;
            imgUrl.value = imgUrlVal;
            if (hotVal == 1) {
                hot1.checked = true;
            } else {
                hot2.checked = true;
            }
        }
    };
    cont.addEventListener("click", editHandle);

    // 编辑按钮
    let alterBtn = function(e) {
        e.preventDefault();

        // 获取表单中的信息
        let titleVal = title.value;
        let imgUrlVal = imgUrl.value;
        let hotVal = hot1.checked ? 1 : 0;

        let url = `update.php?id=${id}&title=${titleVal}&imgUrl=${imgUrlVal}&hot=${hotVal}`;
        ajaxRequest(url, editRenter);

        // 隐藏弹出框
        $(".mask").classList.remove("active");
        $(".add-pannel").classList.remove("active");
        form.reset();
    }
    editBtn.addEventListener("click", alterBtn);
}

// 关闭添加按钮弹出弹出框
function closeBtn() {
    var btn = $(".close");
    btn.onclick = function() {
        $(".mask").classList.remove("active");
        $(".add-pannel").classList.remove("active");
    }
}

loadAll();
selectTitle();
delData();
addData();
closeBtn();
update();