/**
 * 课前小测需求：
 * 1. 页面初始化时拉取所有数据 
 * 2. 搜索功能
 * 3. 添加新商品
 * 4. 删除商品
 */

// 参考代码
// let xhr = new XMLHttpRequest()
// xhr.open("get", `url?a=1&b=1`, true)
// xhr.send()
// xhr.onreadystatechange = function () {
//   if (xhr.readyState == 4 && xhr.status == 200) {
//     let data = JSON.parse(xhr.responseText)
//     console.log(data);
//   }
// }
let cont = $("tbody.cont");

// 数据渲染到页面
function render(data) {
    let html = "";
    if (data.code == 0) {
        // 判断数据是否移除
        if (data.flag) {
            alert("成功移除数据");
            selectAll();
            return;
        }
        // 判断数据是否添加数据
        if (data.addFlag) {
            alert("成功添加数据");
            selectAll();
            return;
        }
        data.list.forEach(item => {
            html += `
             <tr>
                <td>${item.Id}</td>
                <td>${item.title}</td>
                <td>${item.hot}</td>
                <td><img src="https:${item.imgUrl}" alt=""></td>
                <td><button data-id=${item.Id} class="edit">编辑</button></td>
                <td><button data-id=${item.Id} class="del">删除</button></td>
            </tr>    
            `
        })
    } else {
        if (data.flag || data.addFlag) {
            selectAll();
            alert("你没有权限");
            return;
        }
        html = `
            <tr>暂无数据</tr>
        `
    }
    cont.innerHTML = html;
}

// ajax请求数据
function ajaxRequest(url) {
    let xhr = new XMLHttpRequest();
    xhr.open("get", `./api/${url}`, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let data = JSON.parse(xhr.responseText);
            render(data);
            $(".mask").classList.remove("active");
            $(".add-pannel").classList.remove("active");
        }
    }
}

// 初始化加载数据
function selectAll() {
    ajaxRequest("getData.php")
}

// 根据标题搜索显示数据
function selectTitle() {
    let searchBtn = $(".search-btn");
    let title = $(".search-box");

    searchBtn.onclick = function(e) {
        e.preventDefault();
        let titleVal = title.value;
        ajaxRequest(`search.php?search=${titleVal}`);
    }
}

// 移除数据
function delData() {
    cont.onclick = function(e) {
        if (e.target.classList.contains("del")) {
            let id = e.target.dataset.id;
            ajaxRequest(`del.php?id=${id}`);
        }
    }
}

// 添加数据
function addData() {
    let hideBtn = $(".toolbar .add");
    let addBtn = $(".add-pannel .add-btn");
    var title = $(".add-pannel .title");
    var imgUrl = $(".add-pannel .imgUrl");
    var hotChecked = $(".add-pannel .hot");

    hideBtn.onclick = function() {
        $(".mask").classList.add("active");
        $(".add-pannel").classList.add("active");
    }
    addBtn.onclick = function() {
        let titleVal = title.value;
        let imgUrlVal = imgUrl.value;
        let hotCheckedVal = hotChecked.checked ? 1 : 0;
        ajaxRequest(`add.php?title=${titleVal}&imgUrl=${imgUrlVal}&hot=${hotCheckedVal}`);
    }
}
selectAll();
selectTitle();
delData();
addData();