// 函数封装获取dom
function $(selector) {
    let domList = document.querySelectorAll(selector);
    if (domList.length == 0) {
        return null;
    }
    if (domList.length == 1) {
        return domList[0];
    }
    return domList;
}

// 向ajax发起请求
function ajax(url, render) {
    let xhr = new XMLHttpRequest();

    xhr.open('get', url, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let data = JSON.parse(xhr.responseText);
            render(data);
        }
    }
}