// 获取dom函数
function $(selector) {
    var domList = document.querySelectorAll(selector);
    if (domList.length == 0) {
        return null;
    }
    if (domList.length == 1) {
        return domList[0];
    }
    return domList;
}

// 随机数
function rnd(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}