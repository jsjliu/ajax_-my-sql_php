//获取dom
function $(selector) {
    return document.querySelector(selector)
}

function ajax(url, callback) {
    // 如果需要，可以通过在window对象上添加属性的方式把xhr设置为全局变量
    // window.xhr = new XMLHttpRequest()
    let xhr = new XMLHttpRequest()
    xhr.open("get", url, true)
    xhr.send()
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let data = JSON.parse(xhr.responseText)
            callback(data)
        }
    }
}