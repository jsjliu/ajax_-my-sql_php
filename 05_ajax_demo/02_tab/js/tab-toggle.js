let type = "job";

// 将已请求过的数据存入对象当中
loadingData = {};

function render(data) {
    //把第一次请求的数据存入globalData.share
    if (!loadingData[type]) {
        loadingData[type] = data;
        console.log(loadingData);
    }

    let html = '';
    if (data.success) {
        data.data.forEach((item, index) => {
            html += `
             <li>${index+1}、${item.title}--------回复时间：${item.last_reply_at}</li>
            `
        })
    } else {
        html = `
            <li>暂无数据</li>
        `
    }
    $(".list ul").innerHTML = html;

}

// 初始化加载数据；
function initLoad() {
    let url = `https://cnodejs.org/api/v1/topics?tab=${type}&limit=16`;
    ajax(url, render);
    $(".header li").setAttribute("data-isload", true);
}

// 点击按钮切换
function btnToggle() {

    $(".header").onclick = function(e) {
        if (e.target.tagName == "LI") {

            // 通过children,获取所有子元素的集合是伪数组，需要通过es6语法[...]，才能使用forEach方法
            [...e.target.parentNode.children].forEach(itme => {
                itme.classList.remove("active");
            })
            e.target.classList.add("active");

            type = e.target.dataset.type;
            if (e.target.dataset.isload) {
                render(loadingData[type]);
            } else {
                let url = `https://cnodejs.org/api/v1/topics?tab=${type}&limit=16`;
                ajax(url, render);
                e.target.setAttribute("data-isload", true)
            }
        }
    }
}
initLoad();
btnToggle();